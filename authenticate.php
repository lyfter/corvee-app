<?php
session_start();

include('Model.php');

$model = new Model();
$database = $model->getPdo();

if (!isset($_POST['username'], $_POST['password'])) {
    die ('Vul beide velden in!');
}

$username = $_POST['username'];

$hashed_password = hash('sha1', $_POST['password']);

$stm = $database->prepare('SELECT id, username FROM login WHERE username = ? and password = ?');

if ($stm) {
    $stm->execute(array($_POST['username'], $hashed_password));

    if ($stm->rowCount() > 0) {
        $result = $stm->fetch();

        session_regenerate_id();
        $_SESSION['loggedin'] = TRUE;
        $_SESSION['name'] = $result['username'];
        $_SESSION['userId'] = $result['id'];
        header('Location: home.php');

    } else {
        echo 'De gebruikersnaam of het wachtwoord is verkeerd!';
    }
    $model->closeConnection();
}