<?php require "Model.php";
$database = new Model();
session_start();
if (!isset($_SESSION['loggedin'])) {
    header('Location: index.php');
    exit();
} ?>
<link rel="stylesheet" type="text/css" href="style.css">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.1/css/all.css">
<div class="navbar">
    <a href="home.php">Home</a>
    <div class="dropdown">
        <button class="dropbtn">Voorraad
            <i class="fa fa-caret-down"></i>
        </button>
        <div class="dropdown-content">
            <a href="benodigdheden.php">Overzicht en aanpassen</a>
            <a href="benodigdhedenaanmaken.php">Toevoegen</a>
        </div>
    </div>
    <div class="dropdown">
        <button class="dropbtn">Weekoverzicht
            <i class="fa fa-caret-down"></i>
        </button>
        <div class="dropdown-content">
            <a href="weekoverzicht.php">Overzicht en aanpassen</a>
            <a href="weekaanmaken.php">Toevoegen</a>
        </div>
    </div>
    <div class="dropdown">
        <button class="dropbtn">Account
            <i class="fa fa-caret-down"></i>
        </button>
        <div class="dropdown-content">
            <a href="accounts.php">Toevoegen</a>
        </div>
    </div>
    <div class="topnav-right">
        <a href="logout.php">Logout</a>
    </div>
</div>