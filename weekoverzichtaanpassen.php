<!doctype html>
<html>
<head>
    <title>Weekoverzicht wijzigen</title>
</head>
<header>
    <?php include('header.php'); ?>
</header>
<body>
<div class="content">
    <div class="fancysquare">
        <h2 class="bietjemidden">Weekoverzicht aanpassen</h2>

        <?php if (isset($_GET['nummer'])) :
            $number = $_GET['nummer'];
            $week = $database->getWeeksById($number);

            if (!empty($week[0])) : ?>
                <form method="POST">
                    <table>
                        <tr>
                            <td>
                                <div class="tekstkleurke">Week:</div>
                            </td>
                            <td><input type="text" name="txtWeek" value="<?php echo $week[0]['week']; ?>"/></td>
                        </tr>
                        <tr>
                            <td>
                                <div class="tekstkleurke">Naam:</div>
                            </td>
                            <td><input type="text" name="txtNaam" value="<?php echo $week[0]['colleague']; ?>"/></td>
                        </tr>
                        <tr>
                            <td>
                                <div class="tekstkleurke">Opmerkingen dinsdag (max. 500):</div>
                            </td>
                            <td><input type="textarea" name="txtCheckdinsdag"
                                       value="<?php echo $week[0]['checktuesday']; ?>"/>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div class="tekstkleurke">Opmerkingen donderdag (max. 500):</div>
                            </td>
                            <td><input type="textarea" name="txtCheckdonderdag"
                                       value="<?php echo $week[0]['checkthursday']; ?>"/>
                            </td>
                        <tr>
                            <td></td>
                            <td><input type="submit" name="btnUpdate" value="Update"/></td>
                        </tr>
                    </table>
                </form>
            <?php endif; ?>
        <?php endif; ?>

        <?php if (isset($_POST['btnUpdate'])) :
            $mainWeek = $_POST ['txtWeek'];
            $name = $_POST ['txtNaam'];
            $checkTuesday = $_POST ['txtCheckdinsdag'];
            $checkThursday = $_POST ['txtCheckdonderdag'];

            if ($database->updateWeeksById($mainWeek, $name, $checkTuesday, $checkThursday, $number)) : ?>

                <h3>Het item is bijgewerkt.</h3>
                <p><a href="weekoverzicht.php">Ga terug naar het weekoverzicht.</a></p>
            <?php else : ?>
                <h3>Er is iets fout gegaan, probeer het later opnieuw.</h3>
            <?php endif; ?>
        <?php endif; ?>
    </div>
</div>
</body>
</html>