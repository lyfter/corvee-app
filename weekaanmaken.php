<!DOCTYPE html>
<html>
<head>
    <title>Week toevoegen</title>
    <?php include "header.php"; ?>
</head>
<body>
<div class="content">
    <div class="fancysquare">
        <h2 class="bietjemidden">Week toevoegen</h2>
        <h3 class="bietjemidden">Gelieve alleen te gebruiken bij de start van het nieuwe jaar</h3>
        <form method="POST">
            <table>
                <tr>
                    <td><div class="tekstkleurke">Week:</div></td>
                    <td><input type="text" name="txtWeek"/></td>
                </tr>
                <tr>
                    <td><div class="tekstkleurke">Collega:</div></td>
                    <td><input type="text" name="txtCollega"/></td>
                </tr>
                <tr>
                    <td><div class="tekstkleurke">Opmerkingen dinsdag (max. 500):</div></td>
                    <td><input type="text" name="txtDinsdag"/></td>
                </tr>
                <tr>
                    <td><div class="tekstkleurke">Opmerkingen dinsdag (max. 500):</div></td>
                    <td><input type="text" name="txtDonderdag"/></td>
                </tr>
                <tr>
                    <td></td>
                    <td><input type="submit" name="btnOpslaan" value="Opslaan"/></td>
                </tr>
            </table>
        </form>
        <?php if (isset($_POST['btnOpslaan'])) :
            $week = $_POST['txtWeek'];
            $colleague = $_POST['txtCollega'];
            $checktuesday = $_POST['txtDinsdag'];
            $checkthursday = $_POST['txtDonderdag'];

            if ($database->addWeeks($week, $colleague, $checktuesday, $checkthursday)) : ?>

                <h3></h3>
                <p><a href="weekoverzicht.php">Keer terug naar het weekoverzicht.</a></p>
            <?php else : ?>
                <h3>Er is iets fout gegaan, probeer het later opnieuw.</h3>
            <?php endif; ?>
        <?php endif; ?>
    </div>
</div>
</body>
</html>