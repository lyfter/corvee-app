<!DOCTYPE html>
<html>
<head>
    <title>Benodigdheden</title>
    <?php include "header.php"; ?>
</head>
<body>
<div class="content">
    <div class="fancysquare">
        <h2 class="bietjemidden">Voorraad</h2>
        <h3 class="bietjemidden">Klik op het item om deze te bewerken.</h3>
        <?php $supply = $database->getAllSupplies();

        if (!empty($supply)) :
            foreach ($supply as $component) :
                $id = $component['id'];
                $item = $component['item'];
                $amount = $component['amount'];
                $category = $component['category']; ?>
                <hr>
                <table class="tablepadding">
                    <tr>
                        <td colspan="4"><a href="benodigdhedenaanpassen.php?nummer=<?php echo $id; ?>">
                                Item: <?php echo $item; ?>
                            </a></td>
                    </tr>
                    <tr>
                        <td>
                            <div class="tekstkleurke">Hoeveelheid:</div>
                        </td>
                        <td><?php echo $amount; ?></td>
                    </tr>
                    <tr>
                        <td>
                            <div class="tekstkleurke">Categorie:</div>
                        </td>
                        <td><?php echo $category; ?></td>
                    </tr>
                </table>
            <?php endforeach;
        else :
            echo "De voorraad kan niet opgehaald worden";
        endif; ?>
    </div>
</div>
</body>
</html>