<!DOCTYPE html>
<html>
<head>
    <title>Weekoverzicht</title>
    <?php include "header.php"; ?>
</head>
<body>
<div class="content">
    <div class="fancysquare">
        <h2 class="bietjemidden">Weekoverzicht</h2>
        <h3 class="bietjemidden">Klik op het item om deze te bewerken.</h3>
        <?php $weeks = $database->getAllWeeks();

        if (!empty($weeks)) :
            foreach ($weeks as $week) :
                $id = $week['id'];
                $mainWeek = $week['week'];
                $name = $week['colleague'];
                $checkTuesday = $week['checktuesday'];
                $checkThursday = $week['checkthursday']; ?>
                <hr>
                <table>
                    <tr>
                        <td colspan="4"><a href="weekoverzichtaanpassen.php?nummer=<?php echo $id; ?>"> Week: <?php echo $mainWeek; ?>
                            </a></td>
                    </tr>
                    <tr>
                        <td>
                            <div class="tekstkleurke">Naam:</div>
                        </td>
                        <td><?php echo $name; ?></td>
                    </tr>
                </table>
                <table
                <tr>
                    <td>
                        <div class="tekstkleurke">Opmerkingen dinsdag (max. 500):</div>
                    </td>
                    <td><?php echo $checkTuesday; ?></td>
                </tr>
                <tr>
                    <td>
                        <div class="tekstkleurke">Opmerkingen donderdag (max. 500):</div>
                    </td>
                    <td><?php echo $checkThursday; ?></td>
                </tr>
                </table>
                <br><br>
            <?php endforeach;
        else :
            echo "De week lijst kan niet opgehaald worden";
        endif; ?>
    </div>
</div>
</body>
</html>