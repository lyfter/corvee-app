<!doctype html>
<html>
<head>
    <title>Benodigdheden wijzigen</title>
</head>
<header>
    <?php include('header.php'); ?>
</header>
<body>
<div class="content">
    <div class="fancysquare">
        <h2 class="bietjemidden">Benodigdheden wijzigen</h2>

        <?php if (isset($_GET['nummer'])) :
            $number = $_GET['nummer'];

            $supply = $database->getSuppliesById($number);

            if (!empty($supply[0])) : ?>
                <form method="POST">
                    <table>
                        <tr>
                            <td>
                                <div class="tekstkleurke">Item:</div>
                            </td>
                            <td><input type="text" name="txtItem" value="<?php echo $supply[0]['item']; ?>"/></td>
                        </tr>
                        <tr>
                            <td>
                                <div class="tekstkleurke">Hoeveelheid:</div>
                            </td>
                            <td><input type="text" name="txtHoeveelheid" value="<?php echo $supply[0]['amount']; ?>"/>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div class="tekstkleurke">Categorie:</div>
                            </td>
                            <td>
                                <?php $categories = $database->getAllCategories(); ?>
                                <?php if (!empty($categories)) : ?>
                                <div class="custom-select">
                                    <select name="categorieselect" class="select-css" id="categorieselect">
                                        <?php foreach ($categories as $category) : ?>
                                            <?php if ($category['name'] === $supply[0]['category']) : ?>
                                                <option selected="selected"
                                                        value="<?php echo $category['name'] ?>"><?php echo $category['name'] ?></option>
                                            <?php else : ?>
                                                <option value="<?php echo $category['name'] ?>"><?php echo $category['name'] ?></option>
                                            <?php endif; ?>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                                <?php endif; ?>
                            </td>
                        </tr>
                        <tr>
                            <td></td>
                            <td><input type="submit" name="btnUpdate" value="Update"/></td>
                        </tr>
                    </table>
                </form>
            <?php endif; ?>
        <?php endif; ?>

        <?php if (isset($_POST['btnUpdate'])) :
            $item = $_POST['txtItem'];
            $amount = $_POST['txtHoeveelheid'];
            $category = $_POST['categorieselect'];
            $number = $_GET['nummer'];

            if ($database->updateSupplyById($item, $amount, $category, $number)) : ?>

                <h3>Het item is bijgewerkt.</h3>
                <p><a href="benodigdheden.php">Keer terug naar het overzicht van de benodigdheden.</a></p>
            <?php else : ?>
                <h3>Er is iets fout gegaan, probeer het later opnieuw.</h3>
            <?php endif; ?>
        <?php endif; ?>
    </div>
</div>
</body>
</html>