<!DOCTYPE html>
<html>
<head>
    <title>Voorraad uitbreiden</title>
    <?php include "header.php"; ?>
</head>
<body>
<div class="content">
    <div class="fancysquare">
        <h2 class="bietjemidden">Voorraad uitbreiden</h2>
        <form method="POST">
            <table>
                <tr>
                    <td>
                        <div class="tekstkleurke">Item:</div>
                    </td>
                    <td><input type="text" name="txtItem"/></td>
                </tr>
                <tr>
                    <td>
                        <div class="tekstkleurke">Hoeveelheid:</div>
                    </td>
                    <td><input type="text" name="txtHoeveelheid"/></td>
                </tr>
                <tr>
                    <td>
                        <div class="tekstkleurke">Categorie:</div>
                    </td>
                    <td>
                        <?php $categories = $database->getAllCategories(); ?>
                        <?php if (!empty($categories)) : ?>
                            <select name="categorieselect" class="select-css" id="categorieselect">
                                <?php foreach ($categories as $category) : ?>
                                    <option value="<?php echo $category['name'] ?>"><?php echo $category['name'] ?></option>
                                <?php endforeach; ?>
                            </select>
                        <?php endif; ?>
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td><input type="submit" name="btnOpslaan" value="Opslaan"/></td>
                </tr>
            </table>
        </form>
        <?php if (isset($_POST['btnOpslaan'])) :
            $item = $_POST['txtItem'];
            $amount = $_POST['txtHoeveelheid'];
            $category = $_POST['categorieselect'];

            if ($database->addNecessities($item, $amount, $category)) : ?>
                <p><a href="home.php">Ga terug naar de homepage als je niet nog meer product toe wilt voegen.</a></p>
            <?php else : ?>
                <h3>Er is iets fout gegaan, probeer het later opnieuw.</h3>
            <?php endif; ?>
        <?php endif; ?>
    </div>
</div>
</body>
</html>