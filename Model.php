<?php
include 'config.php';

class Model
{
    private $pdo;
    private $config;

    public function __construct()
    {
        global $config;
        $this->config = $config;

        if (!empty($this->config)) {
            $this->dbConnect();
        } else {
            echo "Connection to the database could not be established.";
        }
    }

    /**
     * DB Connect
     */
    private function dbConnect()
    {
        $this->config = json_decode(json_encode($this->config));

        $dsn = "mysql:host={$this->config->host};dbname={$this->config->db};charset={$this->config->charset}";
        $options = [
            PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
            PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
            PDO::ATTR_EMULATE_PREPARES => false,
        ];
        try {
            $this->setPdo(new PDO($dsn, $this->config->user, $this->config->pass, $options));
        } catch (\PDOException $e) {
            throw new \PDOException($e->getMessage(), (int)$e->getCode());
        }
    }

    public function addAccounts($gebruikersnaam, $wachtwoord)
    {
        $query = "INSERT INTO login (username, password) VALUES (?,?)";

        $stm = $this->pdo->prepare($query);
        return $stm->execute(array($gebruikersnaam, $wachtwoord));
    }

    public function addNecessities($item, $amount, $category)
    {
        $query = "INSERT INTO storagesupply (item, amount, category) VALUES (?,?,?)";

        $stm = $this->pdo->prepare($query);
        return $stm->execute(array($item, $amount, $category));
    }

    public function addWeeks($week, $colleague, $checktuesday, $checkthursday)
    {
        $query = "INSERT INTO weeklydistribution (week, colleague, checktuesday, checkthursday) VALUES (?,?,?,?)";

        $stm = $this->pdo->prepare($query);
        return $stm->execute(array($week, $colleague, $checktuesday, $checkthursday));
    }

    /**
     * Gets all the supplies
     *
     * @return array
     */
    public function getAllSupplies()
    {
        $query = "SELECT * FROM storagesupply";

        return $this->doFetchQuery($query);
    }

    /**
     * Gets all the weeks, names and notes
     *
     * @return array
     */
    public function getAllWeeks()
    {
        $query = "SELECT * FROM weeklydistribution";

        return $this->doFetchQuery($query);
    }

    public function getSuppliesById($id)
    {
        $query = "SELECT * FROM storagesupply WHERE id = ?";

        $stm = $this->pdo->prepare($query);
        $stm->execute(array($id));

        return $stm->fetchAll();
    }

    public function getWeeksById($id)
    {
        $query = "SELECT * FROM weeklydistribution WHERE id = ?";

        $stm = $this->pdo->prepare($query);
        $stm->execute(array($id));

        return $stm->fetchAll();
    }

    public function getAllCategories()
    {
        $query = "SELECT * FROM category";

        return $this->doFetchQuery($query);
    }

    /**
     * Updates the supply by selected ID
     *
     * @param $item
     * @param $amount
     * @param $category
     * @param $id
     * @return bool
     */
    public function updateSupplyById($item, $amount, $category, $id)
    {

        $query = "UPDATE storagesupply SET item = ?, amount = ?, category = ? WHERE id = ?";

        $stm = $this->pdo->prepare($query);
        $stm->execute(array($item, $amount, $category, $id));

        return $this->doUpdateQuery($query);
    }

    /**
     *  Update the week by selected ID
     *
     * @param $mainWeek
     * @param $name
     * @param $checkTuesday
     * @param $checkThursday
     * @param $id
     * @return bool
     */
    public function updateWeeksById($mainWeek, $name, $checkTuesday, $checkThursday, $id)
    {
        $query = "UPDATE weeklydistribution SET week = ?, colleague = ?, checktuesday = ?, checkthursday = ? WHERE id = ?";

        $stm = $this->pdo->prepare($query);
        $stm->execute(array($mainWeek, $name, $checkTuesday, $checkThursday, $id));

        return $this->doUpdateQuery($query);
    }

    /**
     * Fetches Query
     *
     * @param $query
     * @return array
     */
    private function doFetchQuery($query)
    {
        $stm = $this->pdo->prepare($query);
        $stm->execute();

        return $stm->fetchAll();
    }

    /**
     * Updates Query
     *
     * @param $query
     * @return bool
     */
    private function doUpdateQuery($query)
    {
        $stm = $this->pdo->prepare($query);

        return $stm->execute();
    }

    /**
     * @return PDO
     */
    public function getPdo()
    {
        return $this->pdo;
    }

    /**
     * @param PDO $pdo
     */
    public function setPdo($pdo)
    {
        $this->pdo = $pdo;
    }

    public function closeConnection()
    {
        $this->closeConnection();
    }
}