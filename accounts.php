<!DOCTYPE html>
<html>
<head>
    <title>Accounts</title>
    <?php include "header.php"; ?>
</head>
<body>
<div class="content">
    <div class="fancysquare">
        <h2 class="bietjemidden">Account aanmaken</h2>
        <form method="POST">
            <table>
                <tr>
                    <td>
                        <div class="tekstkleurke">Gebruikersnaam:</div>
                    </td>
                    <td><input type="text" name="txtGebruikersnaam"/></td>
                </tr>
                <tr>
                    <td>
                        <div class="tekstkleurke">Wachtwoord:</div>
                    </td>
                    <td><input type="password" name="pwWachtwoord"/></td>
                </tr>
                <tr>
                    <td></td>
                    <td><input type="submit" name="btnOpslaan" value="Opslaan"/></td>
                </tr>
            </table>
        </form>
        <?php if (isset($_POST['btnOpslaan'])) :
            $gebruikersnaam = $_POST['txtGebruikersnaam'];
            $hashed_password = hash('sha1', $_POST['pwWachtwoord']);

            if ($database->addAccounts($gebruikersnaam, $hashed_password)) : ?>

                <h3>Het account is toegevoegd aan de database.</h3>
                <p><a href="home.php">Keer terug naar de homepage als je niet nog meer accounts toe wilt voegen.</a></p>
            <?php else : ?>
                <h3>Er is iets fout gegaan, probeer het later opnieuw.</h3>
            <?php endif; ?>
        <?php endif; ?>
    </div>
</div>
</body>
</html>