<?php session_start();
// redirect naar login als je niet bent ingelogd
include('header.php'); ?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Home</title>
</head>
<body class="loggedin">

<div class="login">
    <h1>Home</h1>
    <?php if (!empty($_SESSION['name'])) { ?>
        <div class="loggedintext">Welkom terug, <?php echo $_SESSION['name']; ?>!</div><br/>
    <?php } else {
        echo "De naam kan niet getoond worden.";
    } ?>
</div>
</body>
</html>