<?php
session_start();
session_destroy();
// Redirect naar login na het uitloggen
header('Location: index.php');